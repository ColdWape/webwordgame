﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebWordGame.Models;

namespace WebWordGame.Controllers
{
    public class AdminZoneController : Controller
    {
        private readonly DataBaseContext _dataBaseContext;
        public AdminZoneController(DataBaseContext dataBaseContext)
        {
            _dataBaseContext = dataBaseContext;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult AddNewWordInDataBase()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult AddNewWordInDataBase(string NameOfTheNewWord, string WordForVerifyCorrectName)
        {
            if (NameOfTheNewWord != null || WordForVerifyCorrectName != null)
            {
                NameOfTheNewWord = NameOfTheNewWord.ToLower();
                WordForVerifyCorrectName = WordForVerifyCorrectName.ToLower();

                if (NameOfTheNewWord == WordForVerifyCorrectName)
                {
                    if (_dataBaseContext.Words.FirstOrDefault(w => w.Name == NameOfTheNewWord) == null)
                    {
                        WordModel NewWord = new WordModel
                        {
                            Name = NameOfTheNewWord,
                            NumberOfUses = 1
                        };

                        _dataBaseContext.Words.Add(NewWord);
                        _dataBaseContext.SaveChanges();
                        ViewBag.Alert = "Слово успешно добавлено";
                        return View();

                    }
                    else
                    {
                        ViewBag.Alert = "Слово уже есть в базе";
                        return View();

                    }

                }
                else
                {
                    ViewBag.Alert = "Слова не совпадают";
                    return View();
                }
            }
            else
            {
                ViewBag.Alert = "Оба поля должны быть заполнены";
                return View();
            }
        }
    }
}
