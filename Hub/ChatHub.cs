﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebWordGame.Models;

namespace WebWordGame
{
    public class ChatHub : Hub
    {

        private readonly DataBaseContext _dataBaseContext;

        public ChatHub(DataBaseContext dataBaseContext)
        {
            _dataBaseContext = dataBaseContext;
        }

        public override async Task OnConnectedAsync()
        {
            var userName = Context.User.Identity.Name;
            PersonModel person = _dataBaseContext.People.Include(gamez => gamez.Games).Include(ph => ph.ProfileImageId).FirstOrDefault(u => u.LoginName == userName);


            string groupname = null;
            
            groupname = Convert.ToString(person.LastVisitedRoom);

            var connectionId = Context.ConnectionId;
            RoomGamer roomGamer = _dataBaseContext.RoomGamers.Where(a => Convert.ToString(a.GameId) == groupname).First(i => i.Person == person);
            roomGamer.ConnectedToTheGame = true;
            roomGamer.ConnectId = connectionId;

            GameModel theGame = _dataBaseContext.Games.Include(m => m.Messages).First(g => Convert.ToString(g.Id) == groupname);
            theGame.quantityOfConnectedPeoples += 1;

            _dataBaseContext.SaveChanges();
            if (groupname != null)
            {
                await Groups.AddToGroupAsync(connectionId, groupname);

            }

            if (theGame.GameStatus == "Started")
            {
                foreach (var word in theGame.Messages)
                {
                    if (word.Sender == Context.User.Identity.Name)
                    {
                        await Clients.Client(Context.ConnectionId).SendAsync("UsersWordAdd", word.TextMeassage, word.Sender);
                    }
                    else
                    {
                        await Clients.Client(Context.ConnectionId).SendAsync("OpponentWordAdd", word.TextMeassage, word.Sender);
                    }
                }
            }

           
            await Clients.GroupExcept(groupname, connectionId).SendAsync("ShowNewPerson", person.LoginName, person.ProfileImageId.ImageSource,
                                                                theGame.roomGamers.First(u => u.Person.LoginName == person.LoginName).Score);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            
            
            var userName = Context.User.Identity.Name;
            PersonModel person = _dataBaseContext.People.Include(gamez => gamez.Games).FirstOrDefault(u => u.LoginName == userName);

            string groupname = null;
            
            groupname = Convert.ToString(person.LastVisitedRoom);

            var connectionId = Context.ConnectionId;

            if (groupname != null)
            {
                await Groups.RemoveFromGroupAsync(connectionId, groupname);

                RoomGamer roomGamer = _dataBaseContext.RoomGamers.Where(a => Convert.ToString(a.GameId) == groupname).FirstOrDefault(i => i.Person == person);
                if (roomGamer != null)
                {
                    roomGamer.ConnectedToTheGame = false;
                    _dataBaseContext.Games.FirstOrDefault(g => Convert.ToString(g.Id) == groupname).quantityOfConnectedPeoples--;
                    
                }
                

                await Clients.GroupExcept(groupname, connectionId).SendAsync("HideDisconnectedUser", person.LoginName);

            }

            _dataBaseContext.SaveChanges();

            await base.OnDisconnectedAsync(exception);
        }


    }
}
